#!/usr/bin/env bash

screenuser=$1
screenname=$( date +"%Y-%m-%dT%H:%M:%S%z" ).$screenuser
screenlogpath=$( [[ -d /home/t1bin ]] && echo "/home/t1bin" || echo "/home/tier1adv" )
serverid=$( awk -F '.' '{print $1}' /etc/original_hostname 2> /dev/null || awk -F '/' 'NR == 2 {print "vps"$2; err = 1}; END {exit !err}' /proc/self/cgroup || hostname -s )
screenrc="/tmp/screenrc.$screenuser"

cat > $screenrc << EOL
    layout new

    # change escape key to grave
    escape ``
    
    # logging to tier1adv accessible paths
    logfile ${screenlogpath}/${screenname}.window%n.log
    deflog on
    
    # two terminals, split horizontally
    screen -t Admin      # for root/tier1adv terminals
    screen -t User      # for cPanel user terminals
    
    #incompat with CentOS 6:
    #split -h
    #focus down
    #focus up

    # fix residual TUI output 
    altscreen on
    
    # customized status bar
    hardstatus alwayslastline
    hardstatus string '[ $(echo $serverid) ] [ %= %{-} %-Lw%{= bW}%n%f %t%{-}%+Lw %= ] [ Load: %l ][%Y-%m-%dT%0c:00$( date +"%z")]'
    
    layout save default
    detach
EOL

screen -S $screenname -c $screenrc
pid=$( screen -ls | awk '/\.'"$screenname"'\t/ {print strtonum($1)}' )
screen -x $pid
echo "Detatched from screen $pid, other users can watch your screen windows' outputs with:
tail -f" ${screenlogpath}/${screenname}*
