'''A tool to help determine where to check for large files and folders, so that we can help cleanup high disk usage.'''
import json
import subprocess
import os
import argparse
from tabulate import tabulate

if os.geteuid() != 0:
    exit("This script makes use of WHM's API which only runs as root. Aborting.")

PARSER = argparse.ArgumentParser(
    description='A tool to help determine where to check for large files and folders, so that we can help cleanup high disk usage.',
    prog="largefinder2.py")
PARSER.add_argument(
    "--devices",
    action="store_true",
    help="usage of servers devices, its df -h but less cruft")
PARSER.add_argument(
    "--users",
    action="store_true",
    help="basically WHM List Accounts, when you sort by Disk Used, but also make sure there isn't anything extra taking up space in the /home*/ directories")
PARSER.add_argument(
    "folders",
    type=str,
    nargs='*',
    help="prints disk usage within each folder recursively, but with non-recursive folder sizing")
PARSER.add_argument(
    "-t",
    "--threshold",
    type=int,
    default=100000000,
    help="omit files/folders smaller than this value in bytes, defaults to 100MB")
ARGUMENTS = PARSER.parse_args()

def human_readable(num, suffix='B'):
    '''We work in bytes throughout this script, this fucntion is used to convert to human readable strings for output'''
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0

def usage_per_device():
    '''Retrieves and prints devices/disks on the server and their used/free/total'''
    # from whmapi to
    getdiskusage = json.loads(
        subprocess.Popen(
            "whmapi1 getdiskusage --output=json",
            shell=True,
            stdout=subprocess.PIPE
            ).stdout.read()
        )
    if getdiskusage['metadata']['result'] == 0:
        print("WHM API comamnd " + getdiskusage['metadata']['command'] + " Failed with: \n" + getdiskusage['metadata']['reason'])
    else:
        devices = getdiskusage['data']['partition']

        def kib_to_b(kibibytes):
            '''Used to convert from whmapis kibibytes to bytes'''
            return kibibytes * 1024
        device_usage = {
            "headers":
                [
                    'Used',
                    'Mount',
                    'Free',
                    'Total',
                    'Used%'
                ],
            "data":
                [
                    [
                        kib_to_b(device['used']),
                        device['mount'],
                        kib_to_b(device['available']),
                        kib_to_b(device['total']),
                        device['percentage']
                    ] for device in devices if "tmp" not in device['mount']
                ]
            }

        # to Human Readable, for "Used, Available, Total" columns
        for column in [0, 2, 3]:
            for place, value in enumerate([i[column] for i in device_usage['data']]):
                device_usage['data'][place][column] = human_readable(value)

        print tabulate(device_usage['data'],
                    headers=device_usage['headers'])


def usage_per_cpanel_account():
    '''Retrieves cPanel user list, sorts them by reported disk size, and uses this list to find extraneous homedir files/folders'''
    def mib_to_b(mebibytes):
        '''whmapi listaccts gets us disk usage in MiB plus "M" making it a string, this strips the M and converts to integer'''
        return int(mebibytes[:-1]) * 1024 * 1024

    getlistaccts = json.loads(
        subprocess.Popen(
            "whmapi1 listaccts want=diskused,user,disklimit,partition --output=json",
            shell=True,
            stdout=subprocess.PIPE
            ).stdout.read()
        )
    if getlistaccts['metadata']['result'] == 0:
        print("WHM API comamnd " + getlistaccts['metadata']['command'] + " Failed with: \n" + getlistaccts['metadata']['reason'])
    else:
        cpanel_users = getlistaccts['data']['acct']
        users_usage = {
            "headers":
                [
                    'Used',
                    'User',
                    'Quota',
                    'Partition',
                ],
            "data":
                [
                    [
                        mib_to_b(cpanel_user['diskused']),
                        cpanel_user['user'],
                        cpanel_user['disklimit'],
                        "/" + cpanel_user['partition'] + "/"
                    ] for cpanel_user in cpanel_users
                ]
            }

        users_usage['data'].sort(key=lambda a: a[0], reverse=True)

        # to Human Readable, for "Used" column
        for place, value in enumerate([i[0] for i in users_usage['data']]):
            users_usage['data'][place][0] = human_readable(value)

        print tabulate(users_usage['data'], headers=users_usage['headers'])

        # now that weve determined usage of cPanel users within their own homedirs, lets make sure there isnt any extra files/folders sitting in these homedirs taking up space

        homedirs = [user[3] for user in users_usage['data']]
        # remove dupliates from list
        homedirs = list(dict.fromkeys(homedirs))
        # now sort them alphabetically
        homedirs.sort()

        # check for extraneous files/folders for each homedir
        for homedir in homedirs:
            dir_objects = os.listdir(homedir)

            files_in_dir = [fs_object for fs_object in dir_objects if os.path.isfile(homedir + fs_object)]

            if files_in_dir:
                # add back in homedir path to build an absolute path
                files_in_dir = [homedir + filename for filename in files_in_dir]

                # list to space separated literal string we will be sending to du
                files_in_dir = " ".join(files_in_dir)

                print "\nUsage of extraneous files in " + homedir
                usage_within_folder(files_in_dir)

            folders_in_dir = [fs_object for fs_object in dir_objects if os.path.isdir(homedir + fs_object)]

            if folders_in_dir:
                # strip /home/virtfs/ itself from our list since it doesnt take up real space
                folders_in_dir = [foldername for foldername in folders_in_dir if foldername != "virtfs"]

                # build list of legitimate cPanel home folders in this homedir
                homedir_cpusers = [user[1] for user in users_usage['data'] if user[3] == homedir]

                # strip these legitimate home folders from our folders to check
                folders_in_dir = [foldername for foldername in folders_in_dir if foldername not in homedir_cpusers]

                # add back homedir path to build an absolute path
                folders_in_dir = [homedir + foldername for foldername in folders_in_dir]

                # list to space separated literal string we will send to du to check them all at once
                folders_in_dir = " ".join(folders_in_dir)

                print "\nUsage of extraneous subfolders within " + homedir
                usage_within_folder(folders_in_dir)

# usage for each path passed as argument
def usage_within_folder(what_to_du):
    '''Wraps around du util to check for large folders and files, called by arguments and Usage_Per_cPanel_Account()'''
    folder_du = subprocess.Popen('du -cabS ' + what_to_du + ' | sort -hr', shell=True, stdout=subprocess.PIPE).stdout.read()
    folder_usage = {
        "headers": [
            'Used',
            'Path',
            ],
        "data": [
            subfolder_usage.split() for subfolder_usage in folder_du.splitlines()
            ]
        }

    # filter out small folders
    folder_usage['data'] = [
        subfolder_usage for subfolder_usage in folder_usage["data"] if str(subfolder_usage[1]) == "total" or int(subfolder_usage[0].strip()) > ARGUMENTS.threshold
    ]

    # to Human Readable, for "Used" column
    for place, value in enumerate([i[0] for i in folder_usage['data']]):
        folder_usage['data'][place][0] = human_readable(int(value))

    print tabulate(folder_usage['data'])

print "LARGE FILE/FOLDER LOCATOR 2: ELECTRIC BOOGALOO"

if ARGUMENTS.devices:
    print "\nUsage per Device:"
    usage_per_device()
else:
    print "\n--devices flag missing, skipping that..."

if ARGUMENTS.users:
    print "\nUsage per cPanel Account:"
    usage_per_cpanel_account()
else:
    print "\n--users flag missing, skipping that..."

for folder in ARGUMENTS.folders:
    print "\nUsage within " + folder
    usage_within_folder(folder)
