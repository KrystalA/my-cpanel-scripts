# How to use these scripts
Each script, denotated by their extension, should be run using the right interpreter. For example, .sh scripts should be ran with /bin/bash.

### Install to server you're running on
```
git clone https://gitlab.com/Zebouski/my-cpanel-scripts.git
/path/to/intepreter ./my-cpanel-scripts/script_name -flags args
```

### Run without installing 
```
curl -s https://gitlab.com/Zebouski/my-cpanel-scripts/blob/master/script_name > /tmp/script_name
/path/to/intepreter /tmp/script_name -flags args
```
Or leave even less of a trace by not writing a real file:
```
/path/to/intepreter <(curl -s https://gitlab.com/Zebouski/my-cpanel-scripts/blob/master/script_name) -flags args
```
### Copy paste the scripts in into your SSH terminal
This process substitution method can be taken further by running cat on a heredoc, with the full code within. Place the interpeter up front, give the code to it as a file, and add the flags and arguments at the end:
```
/path/to/intepreter <( cat << END
PLACE CODE FROM REPO SCRIPT HERE
END
) -flags args
```

So that you don't have to keep copy+pasting these from a file to your terminal manually, consider setting these up in a texter program:

#### AutoKey
Clone the repo to your own computer instead, and give the AutoKey Phrase the path that it'll fill in from:
```
/path/to/intepreter <( cat << END
<file name=/home/zebw/Git/my-cpanel-scripts/mutiphp_manager.sh>
END
) -flags args
```

#### AutoHotKey
idk man