#!/usr/bin/env bash
######################## 
# REQUIRED ARGUMENTS:  #
# 1) Database Name     #
# 2) Database Username #
#                      #
# Optional Arguments:  #
# 3) Database Password #
########################

#Check if user is root.  If user is root, fail with warning.
if [[ $EUID -eq 0 ]]; then
    echo "This script must not be run as root, but instead as the cPanel user you wish to work on."
    exit 1
fi

set -e

#Grab Mysql restrictions from UAPI. (DB Name length, UserName length, and Prefix.)
mysql_restrictions=$( mktemp )
uapi Mysql get_restrictions > $mysql_restrictions

#Set the MySql database prefix.
mysql_prefix=$( awk '$1 == "prefix:" {print $2}' $mysql_restrictions )

#Set the DB and User Name. 
dbname=${mysql_prefix}$( cut -c1-$( awk '$1 == "max_database_name_length:" {print $2}' $mysql_restrictions ) <<< $1 )
dbuser=${mysql_prefix}$( cut -c1-$( awk '$1 == "max_username_length:" {print $2}' $mysql_restrictions ) <<< $2 )

#If the Database Password was passed as an argument to this script, use that. 
#If Not, generate a random AlphaNumerical password.
dbpass=${3:-$( openssl rand -base64 12 | tr -cd '[:alnum:]' )}
#If database Password is not Alphanumerical make user aware that issues may result.
[[ $dbpass =~ ^[[:alnum:]]*$ ]] \
    || echo -e "\e[31mPassword is not alphanumeric, issues setting password may be encountered.\e[39m"


echo -e "For cPanel user $USER, creating MySQL database \e[33m$dbname\e[39m managed by MySQL username \e[33m$dbuser\e[39m using password \e[33m$dbpass\e[39m using these uapi commands:"


return=$( mktemp )
(
    set -x
    #Generate Mysql Database, user, and apply priviliges.
    uapi Mysql create_database name=$dbname 2> /dev/null
    uapi Mysql create_user name=$dbuser password="${dbpass}" 2> /dev/null
    uapi Mysql set_privileges_on_database database=$dbname user=$dbuser privileges="ALL PRIVILEGES" 2> /dev/null
) > $return

#Test That Database creds work, and if not, alert the user.
mysql -p${dbpass} -u $dbuser $dbname -e ";" 2> /dev/null \
    && echo -e "\e[32mGenerated MySQL credentials confirmed working.\e[39m" \
    ||  {
            echo -e "\e[31mGenerated MySQL credentials not working, errors from uapi:\e[39m";
            awk 'P && /messages:/ { P=0 }; P; /errors:/ { P=1 }' $return
        }
